# Z80 Bus Monitor

With this bus monitor you can monitor the address and databus, together with all signals, for a Z80 processor. It also acts as memory to 'feed' the CPU with data.

The goal is to fill real, physical memory with data from an SD-card and then boot the Z80 with it. It will be a fun journey.

![](images/setup.jpeg)

## Features
- RAM memory to read from and write to, which can be prefilled (hardcoded)
- OLED display to show different pages with info (can be cycled through using a button)
- Displays all signals
- Displays addressbus contents
- Displays databus contents
- RGB led for showing context-depending info. Eg. when the 'IO WRITE' page is show on the OLED, it flashes red when on IO write request is being made
- Resets fatal erros on presssing Reset
- Relaxed license

## Arduino Dependencies
- ArduinoJson library, https://arduinojson.org/

These are easy to install in the Arduino IDE:
- Sketch / Include Library / Manage Libraries

## BOM
- [Robotdyn MegaXPro 2560 R3](https://www.aliexpress.com/wholesale?SearchText=Robotdyn+MegaXPro+2560+R3) or similar
- [RC2014 protoytype PCB](https://www.tindie.com/products/semachthemonkey/prototype-pcb-for-rc2014/)
- [SSD1306 OLED screen 128x32 I2C](https://www.aliexpress.com/wholesale?SearchText=oled+128x32+I2C+SSD1306)
- 90 degrees 2x40 (double) header pins
- An RC2014 or similar backplane. I use the Pro: https://www.tindie.com/products/semachthemonkey/backplane-pro-for-rc2014-z80-homebrew-computer/

## Assembling guidelines
To replicate this project:

- Wire according to the pin definitions (see PinDefinitions.h)
- I also soldered headers on the RC2014 module PCB and am using DuPont cables to wire everything together
- Don't forget to also connect 5V and GND
- Connect an 128x32 OLED screen via I2C
- Connect a button according to the pin definitions. The button is used to select a different page on the OLED screen
- Connect a (Micro) SD-card breakout module to the Arduino (I followed this tutorial: https://randomnerdtutorials.com/guide-to-sd-card-module-with-arduino/)
- Be sure there is also a Z80 CPU and a slow clock connected to the backplane. Check out https://gitlab.com/z80a/arduino-clock-generator for a slow clock with buttons for stepping through each cycle
- I have it working stable up to ~1000 Hz
- RAM or ROM memory should not be connected as this module also acts as memory (this will be configurable in the future)
- Your favourite drugs (caffeine or maybe Zen meditation)

![](images/wire%20mayhem.jpeg)

## Usage instructions
- Connect to a RC2014 backplane
- After powering in reset for at least 3 cycles. In Z80Memory.h file you can change the contents of the memory. There are some examples commented out
- Press the button to change the OLED screen
- Press and hold the button to disable/enable the flashing RGB led

## Boot strapping (WIP!)

(this is under development and not working yet)

The Bus Monitor can also fill a memory with data and let the Z80 boot from it. To do that:

- Assemble a MEMORY.BIN with your favourite routines and/or rom in the root of an SD-card
- Note that it doesn't get to big. 2~4KB should fit.
- Put the SD-card in the Bus Module & reboot
