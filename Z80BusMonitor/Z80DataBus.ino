#include "Z80DataBus.h"

void Z80DataBus::write(uint8_t data) {
  pinMode(DATA0, OUTPUT);
  pinMode(DATA1, OUTPUT);
  pinMode(DATA2, OUTPUT);
  pinMode(DATA3, OUTPUT);
  pinMode(DATA4, OUTPUT);
  pinMode(DATA5, OUTPUT);
  pinMode(DATA6, OUTPUT);
  pinMode(DATA7, OUTPUT);
    
  digitalWrite(DATA0, (data & (1<<0))?HIGH:LOW);
  digitalWrite(DATA1, (data & (1<<1))?HIGH:LOW);
  digitalWrite(DATA2, (data & (1<<2))?HIGH:LOW);
  digitalWrite(DATA3, (data & (1<<3))?HIGH:LOW);
  digitalWrite(DATA4, (data & (1<<4))?HIGH:LOW);
  digitalWrite(DATA5, (data & (1<<5))?HIGH:LOW);
  digitalWrite(DATA6, (data & (1<<6))?HIGH:LOW);
  digitalWrite(DATA7, (data & (1<<7))?HIGH:LOW);
}

uint8_t Z80DataBus::read() {
  int data0 = digitalRead(DATA0);
  int data1 = digitalRead(DATA1);
  int data2 = digitalRead(DATA2);
  int data3 = digitalRead(DATA3);
  int data4 = digitalRead(DATA4);
  int data5 = digitalRead(DATA5);
  int data6 = digitalRead(DATA6);
  int data7 = digitalRead(DATA7);
  uint8_t data = data0 | data1 << 1 | data2 << 2 | data3 << 3 | data4 << 4 | data5 << 5 | data6 << 6 | data7 << 7;

//  Serial.println(F("Read: "));
//  Serial.println(data);
  return data;
}
