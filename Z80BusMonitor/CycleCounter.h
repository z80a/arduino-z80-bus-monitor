#ifndef CycleCounter_h
#define CycleCounter_h

class CycleCounter {
  public:
    CycleCounter();
    void setupInterrupt();
    
    static bool m1CycleActive;
    static bool lastM1State;
    static int tState;

    static void clockCycleInterrupt();
};

#endif
