#ifndef ButtonState_h
#define ButtonState_h

typedef enum ButtonState: int {
  NotPressed = 0,
  ShortPress = 1,
  LongPress = 2
} ButtonState_t;

#endif
