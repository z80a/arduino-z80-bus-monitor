#ifndef Z80BusMonitor_h
#define Z80BusMonitor_h

#include "Z80DataBus.h"
#include "Z80AddressBus.h"
#include "Z80Memory.h"

class Z80BusMonitor {
  private:
    void initializePins();

    int resetTimer = 0;
    int resetDuration = 500; // ms

    int intrTimer = 0;
    int intrDuration = 2000; // ms

    int waitTimer = 0;
    int waitDuration = 5000; // ms

  public:
    Z80BusMonitor();
    void begin();

    void startReset();
    void handleReset();
    void startIntr();
    void handleIntr();
    void startWait();
    void handleWait();
    void resetMemory();

    Z80DataBus dataBus = Z80DataBus();
    Z80AddressBus addressBus = Z80AddressBus();
    Z80Memory memory = Z80Memory();
};
#endif
