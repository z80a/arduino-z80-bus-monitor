#ifndef Z80AddressBus_h
#define Z80AddressBus_h

class Z80AddressBus {
  public:
    uint16_t read();
    void write(uint16_t address);
    bool claim();
    void unclaim();
};

#endif
