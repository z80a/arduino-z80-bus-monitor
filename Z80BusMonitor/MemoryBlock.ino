#include "MemoryBlock.h"
#include "Console.h"

MemoryBlock::MemoryBlock() {
  
}

MemoryBlock::MemoryBlock(String filename, uint16_t startAddress) {
  this->filename = filename;
  this->startAddress = startAddress;
}

bool MemoryBlock::addressIsInBlock(uint16_t address) {
  return ((address >= startAddress) && (address < startAddress + size));
}

bool MemoryBlock::read(uint16_t address, uint8_t* data) {
  if (!addressIsInBlock(address)) return false;

  uint16_t relativeAddress = address - startAddress;
  *data = contents[relativeAddress];
}
