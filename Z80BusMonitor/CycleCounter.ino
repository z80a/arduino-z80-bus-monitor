#include "CycleCounter.h"

static bool CycleCounter::m1CycleActive = false;
static bool CycleCounter::lastM1State = true;
static int CycleCounter::tState = 0;
    
void CycleCounter::setupInterrupt() {
    attachInterrupt(digitalPinToInterrupt(CLK), CycleCounter::clockCycleInterrupt, RISING);
}

CycleCounter::CycleCounter() {
  setupInterrupt();
}

static void CycleCounter::clockCycleInterrupt() {

  bool m1Active = !digitalRead(M1); // active low

  // Start of M1 cycle
  if (m1Active && m1Active != lastM1State) {
    lastM1State = m1Active;
    tState = 1;
    return;
  } else {
    tState++;
  }

  // Reset M1
  if (!m1Active && m1Active != lastM1State) {
    lastM1State = m1Active;
  }

  
  
}
