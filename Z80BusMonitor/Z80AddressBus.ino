#include "Z80AddressBus.h"

void Z80AddressBus::write(uint16_t address) {
  pinMode(ADDRESS0, OUTPUT);
  pinMode(ADDRESS1, OUTPUT);
  pinMode(ADDRESS2, OUTPUT);
  pinMode(ADDRESS3, OUTPUT);
  pinMode(ADDRESS4, OUTPUT);
  pinMode(ADDRESS5, OUTPUT);
  pinMode(ADDRESS6, OUTPUT);
  pinMode(ADDRESS7, OUTPUT);
  pinMode(ADDRESS8, OUTPUT);
  pinMode(ADDRESS9, OUTPUT);
  pinMode(ADDRESS10, OUTPUT);
  pinMode(ADDRESS11, OUTPUT);
  pinMode(ADDRESS12, OUTPUT);
  pinMode(ADDRESS13, OUTPUT);
  pinMode(ADDRESS14, OUTPUT);
  pinMode(ADDRESS15, OUTPUT);
    
  digitalWrite(ADDRESS0, (address & (1<<0))?HIGH:LOW);
  digitalWrite(ADDRESS1, (address & (1<<1))?HIGH:LOW);
  digitalWrite(ADDRESS2, (address & (1<<2))?HIGH:LOW);
  digitalWrite(ADDRESS3, (address & (1<<3))?HIGH:LOW);
  digitalWrite(ADDRESS4, (address & (1<<4))?HIGH:LOW);
  digitalWrite(ADDRESS5, (address & (1<<5))?HIGH:LOW);
  digitalWrite(ADDRESS6, (address & (1<<6))?HIGH:LOW);
  digitalWrite(ADDRESS7, (address & (1<<7))?HIGH:LOW);
  digitalWrite(ADDRESS8, (address & (1<<8))?HIGH:LOW);
  digitalWrite(ADDRESS9, (address & (1<<9))?HIGH:LOW);
  digitalWrite(ADDRESS10, (address & (1<<10))?HIGH:LOW);
  digitalWrite(ADDRESS11, (address & (1<<11))?HIGH:LOW);
  digitalWrite(ADDRESS12, (address & (1<<12))?HIGH:LOW);
  digitalWrite(ADDRESS13, (address & (1<<13))?HIGH:LOW);
  digitalWrite(ADDRESS14, (address & (1<<14))?HIGH:LOW);
  digitalWrite(ADDRESS15, (address & (1<<15))?HIGH:LOW);
}

uint16_t Z80AddressBus::read() {
  int address0 = digitalRead(ADDRESS0);
  int address1 = digitalRead(ADDRESS1);
  int address2 = digitalRead(ADDRESS2);
  int address3 = digitalRead(ADDRESS3);
  int address4 = digitalRead(ADDRESS4);
  int address5 = digitalRead(ADDRESS5);
  int address6 = digitalRead(ADDRESS6);
  int address7 = digitalRead(ADDRESS7);
  int address8 = digitalRead(ADDRESS8);
  int address9 = digitalRead(ADDRESS9);
  int address10 = digitalRead(ADDRESS10);
  int address11 = digitalRead(ADDRESS11);
  int address12 = digitalRead(ADDRESS12);
  int address13 = digitalRead(ADDRESS13);
  int address14 = digitalRead(ADDRESS14);
  int address15 = digitalRead(ADDRESS15);
  
  return address0 | address1 << 1 | address2 << 2 | address3 << 3 | address4 << 4 | address5 << 5 | address6 << 6 | address7 << 7 | address8 << 8 | address9 << 9 | address10 << 10 | address11 << 11 | address12 << 12 | address13 << 13 | address14 << 14 | address15 << 15;
}

bool claim() {
  digitalWrite(BUSRQ, LOW);

  for (int i=0; i<10; i++) {
    bool busAck = !digitalRead(BUSACK);
    if (busAck) {
      return true;
    }
    delay(10);
  }
  return false;
}


void unclaim() {
  digitalWrite(BUSRQ, HIGH);
}
