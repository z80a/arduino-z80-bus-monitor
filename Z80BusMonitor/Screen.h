#ifndef Screen_h
#define Screen_h

typedef enum Screen: int {
  ScreenMin = 0,
  ScreenCycle = 0,
  ScreenMemoryRead = 1,
  ScreenMemoryWrite = 2,
  ScreenIoRead = 3,
  ScreenIoWrite = 4,
  ScreenMax = 4
} Screen_t;

#endif
