#ifndef IoPort_h
#define IoPort_h

class IoPort {
  public:    
    explicit IoPort(uint8_t portNumber);
    virtual uint8_t read();
    virtual void write(uint8_t data);
    
  protected:
     uint8_t portNumber;
};
#endif
