#ifndef SerialBridgeStatusPort_h
#define SerialBridgeStatusPort_h

#include "IoPort.h"

typedef enum class SerialBridgeCommand: uint8_t {
  DoNothing = 0,
  Write = 1,
  ClearScreen = 2,
  Attention = 3
} SerialBridgeCommand_t;

class SerialBridgeStatusPort: IoPort {
  using IoPort::IoPort;
  
  public:
    uint8_t read();
    void write(uint8_t data);

  private:
    void handleCommand(SerialBridgeCommand command);
};

#endif
