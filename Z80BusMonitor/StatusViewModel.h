

struct StatusViewModel {
  bool m1 = false;
  int tState = 0;
  bool intr = false;
  bool rd = false;
  bool wr = false;
  bool nmi = false;
  bool iorq = false;
  bool mreq = false;
  bool halt = false;
  bool clk = false;
  bool reset = false;
  bool refresh = false;
  bool ledsEnabled = true;
  bool wait = false;
  bool busrq = false;
  bool busack = false;
  bool intrAck = false;
} StatusViewModel_t;
