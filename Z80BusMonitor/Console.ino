#include "Console.h"
#include "MonitorDisplay.h"


static void Console::printHex8(uint8_t *data, uint8_t length) // prints 8-bit data in hex with leading zeroes
{
       for (int i=0; i<length; i++) { 
         if (data[i]<0x10) {Serial.print(F("0"));} 
         Serial.print(data[i],HEX); 
         Serial.print(F(" ")); 
       }
}

static void Console::printHex16(uint16_t *data, uint8_t length) // prints 16-bit data in hex with leading zeroes
{
       for (int i=0; i<length; i++)
       { 
         uint8_t MSB=byte(data[i]>>8);
         uint8_t LSB=byte(data[i]);
         
         if (MSB<0x10) {Serial.print(F("0"));} Serial.print(MSB,HEX); Serial.print(F(" ")); 
         if (LSB<0x10) {Serial.print(F("0"));} Serial.print(LSB,HEX); Serial.print(F(" ")); 
       }
}

Console::Console(MonitorDisplay& oled) {
  oled = oled;
}

void Console::begin() {
    Serial.begin(115200);  
    Serial.print(F("\n\nZ80 Bus Monitor version "));
    Serial.println(programVersion);
    Serial.println(F("Copyright 2020 by The Convenience Factory"));
    Serial.print(free_memory()); Serial.println(F(" bytes free"));
}

void Console::prompt() {
  Serial.print(F("\n"));

  switch (lastResult) {
    case Ok:
      Serial.print(F("OK"));
      break;
    case SyntaxError:
      Serial.print(F("Syntax Error"));
      break;
    case IncorrectParameter:
      Serial.print(F("Incorrect parameter"));
      break;
  }
  
  lastResult = Ok;
  Serial.print(F("\nZ80BusMonitor> "));

}

void Console::help() {
  Serial.println(F("Available commands:\n"));
  for (int i=0; i<numberOfCommands; i++) {
      ConsoleCommand command = commands[i];
      Serial.print("\t");
      Serial.print(command.shortcut);
      Serial.print("\t");
      Serial.print(command.mnemonic);
      Serial.print("\t");
      Serial.print(command.description);
      Serial.print("\n");
  }
}

void Console::handleCommand(String command, bool alreadyHandled) {
  if (command == "?") {
    help();
    lastResult = Ok;  
  } else if (command == "n") {
    oled->nextScreen();
    lastResult = Ok;  
  } else if (command == "p") {
//    oled->nextPage();
    lastResult = Ok;  
  } else if (!alreadyHandled) {
    lastResult = SyntaxError;
  }

  this->command="";
  prompt();
}

bool Console::read(String& command) {
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    this->command += (char)incomingByte;

    if (incomingByte == 10) {
      this->command.trim();
      command = this->command;
      return true;
    }
  }
  
  return false;
}

static void Console::log(String message) {
  Serial.print(message);
}

static void Console::error(String message) {
  Serial.print(message);
}
