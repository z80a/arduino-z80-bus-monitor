#ifndef Button_h
#define Button_h

#include "ButtonState.h"

class Button {
      private:
        bool _state;
        uint8_t _pin;
        unsigned long buttonPressTime = 0;
        unsigned long longPressTime = 350;
        
        unsigned long lastDebounceTime = 0;
        unsigned long debounceDelay = 0;

      public:
        Button(uint8_t);
        void begin();
        ButtonState buttonState();
};

#endif
