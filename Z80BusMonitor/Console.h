#ifndef Console_h
#define Console_h

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__
 
int free_memory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}

typedef enum ConsoleErrorState: int {
  Ok = 0,
  SyntaxError = 1,
  IncorrectParameter = 2
} ConsoleErrorState_t;


struct ConsoleCommand {
  char shortcut;
  String mnemonic;
  String description;
};

class MonitorDisplay;

  class Console {
      private:
        byte incomingByte = 0;
        void help();
        ConsoleErrorState_t lastResult = Ok;

        const static int PROGMEM numberOfCommands = 1;
        const ConsoleCommand commands[numberOfCommands] PROGMEM = { // PROGMEM doesn't do much here
//          { 'x',    "eXit",    "Exit to Z80 console (data on part XX will be mirrored)" },
//          { 's',    "Screen",  "Next screen on OLED display" },
//          { 'p',    "Page",    "Next page on OLED display" },
//          { 't',    "Toggle",  "Toggle LED feedback" },
          { 'b',    "Block",   "Show overview of loaded memory blocks" },
        };
          
        String command = "";

      public:
        Console(MonitorDisplay& oled);
        void begin();
        bool read(String& command);
        void prompt();
        void handleCommand(String commmand, bool alreadyHandled);

        static void printHex8(uint8_t *data, uint8_t length);
        static void printHex16(uint16_t *data, uint8_t length);

        MonitorDisplay* oled;

        static void Console::log(String message);
        static void Console::error(String message);

        
};

#endif
