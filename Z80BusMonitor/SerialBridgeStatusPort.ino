#include "SerialBridgeStatusPort.h"

void SerialBridgeStatusPort::handleCommand(SerialBridgeCommand command) {
  switch (command) {
    case SerialBridgeCommand::Write:
      break;
  }
}

uint8_t SerialBridgeStatusPort::read() {
  return 0x42; // TODO: return number of bytes available
}

void SerialBridgeStatusPort::write(uint8_t data) {
  handleCommand(SerialBridgeCommand(data));
}
