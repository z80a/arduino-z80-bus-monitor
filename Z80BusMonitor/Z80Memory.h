#ifndef Z80Memory_h
#define Z80Memory_h

#include "SerialBridgeStatusPort.h"
#include "MemoryBlock.h"
#include "Console.h"
#include "InstructionData.h"

typedef enum class IoPortNumber: uint8_t {
  SerialBridgeStatus  = 0x02,
  SerialBridgeData    = 0x03
} IoPort_t;


InstructionData memoryReadData;
InstructionData memoryWriteData;
InstructionData ioReadData;
InstructionData ioWriteData;
InstructionData lastBusData;

class Z80Memory {
  public:
    Z80Memory();
    void begin();
    void reset();

    static int numberOfBlocks;
    static MemoryBlock blocks[16];

private:
    static bool Z80Memory::blockFromAddress(uint16_t address, MemoryBlock** blockFound);

    void setupMemory();
    void setupInterrupts();
    void parseConfig(String filename);
    void readBinary(MemoryBlock& memoryBlock);
    void copyBinaryToMemory(String filename, uint16_t startAddress);

    static SerialBridgeStatusPort serialBridgeStatusPort;

    static void memoryRequestInterrupt();
    static void ioRequestInterrupt();

    static uint8_t readInput(uint8_t rawPortNumber);
    static void writeOutput(uint8_t rawPortNumber, uint8_t data);

    /*
     * Doing nothing
     *
     * nop    ; = 00
     *
     */
//    static const uint8_t RamSize = 1;
//    unsigned char ram[RamSize]={ 0x0 };

    /*
     * Halt
     *
     * halt   ; = 0x76
     *
     */
//    static const uint8_t RamSize = 5;
//    unsigned char ram[RamSize]={ 0x0, 0x0, 0x0, 0x0, 0x76 };

   /*
     *  Memory write request
     *
     * ld hl, $5555    ; = 21 ff ff ; Content ff ff
     * ld (**),hl      ; = 22 55 55 ; Load ff ff into 55 55
     * nop
     * nop
     * nop
     * nop
     * ld $00,hl   ; = 21 00 00
     * jp (hl)     ; = e9
     *
     */
    static const uint8_t RamSize = 14;
    unsigned char ram[RamSize]={ 0x21, 0xff, 0xff, 0x22, 0x55, 0x55, 0x0, 0x0, 0x0, 0x0, 0x21, 0x00, 0x00, 0xe9 };

   /*
     *  I/O read request
     *
     * in a,(*)    ; = db 03  ; Port 0x3
     * nop
     * nop
     * nop
     * nop
     * ld $00,hl   ; = 21 00 00
     * jp (hl)     ; = e9
     *
     */
//    static const uint8_t RamSize = 12;
//    unsigned char ram[RamSize]={ 0xdb, 0x03, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x21, 0x00, 0x00, 0xe9 };

    /*
     *  I/O write request
     *
     * ld a, 'A'    ;             = 3e 41
     * out (*), a   ; port 0x03   = d3 03
     * nop
     * nop
     * nop
     * nop
     * ld $00,hl   ; = 21 00 00
     * jp (hl)     ; = e9
     *
     */
//    static const uint8_t RamSize = 13;
//    unsigned char ram[RamSize]={ 0x3d, 0x65, 0xd3, 0x03, 0x0, 0x0, 0x0, 0x0, 0x0, 0x21, 0x00, 0x00, 0xe9 };

    /*
     * Jump to 0x00ff
     *
     * ld $ff,hl    ; = 21 ff 00
     * jp (hl)      ; = e9
     */
//    static const uint8_t RamSize = 6;
//    unsigned char ram[RamSize]={ 0x21, 0xff, 0x00, 0xe9, 0xc9, 0x29 };


//     // 0A 00 00 00 21 09 00 AF 3C 77 C3 04 00 00 A3
//    static const uint8_t RamSize = 14;
//    unsigned char ram[RamSize]={ 0x0a, 0x0, 0x00, 0x21, 0x09, 0x00, 0xaf, 0x3c, 0x77, 0xc3, 0x04, 0x00, 0x00, 0xa3 };



};

#endif
