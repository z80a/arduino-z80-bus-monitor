#ifndef PinDefinitions_h
#define PinDefinitions_h

#define ADDRESS0 A0
#define ADDRESS1 A1
#define ADDRESS2 A2
#define ADDRESS3 A3
#define ADDRESS4 A4
#define ADDRESS5 A5
#define ADDRESS6 A6
#define ADDRESS7 A7
#define ADDRESS8 78
#define ADDRESS9 79
#define ADDRESS10 80
#define ADDRESS11 81
#define ADDRESS12 82
#define ADDRESS13 83
#define ADDRESS14 84
#define ADDRESS15 85

// 0 I2C DATA
// 1 I2C CLOCK

#define DATA0 2
#define DATA1 3
#define DATA2 4
#define DATA3 5
#define DATA4 6
#define DATA5 7
#define DATA6 8
#define DATA7 9

// Not all pins are suitable for interrupts
// See: https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
// Mega, Mega2560, MegaADK: 2, 3, 18, 19, 20, 21, 43, 44, A13, A14 https://robotdyn.com/pub/media/GR-00000194==MegaXPRO-ATmega2560-CH340C/DOCS/PINOUT==GR-00000194==MegaXPRO-ATmega2560-CH340C==MicroUSB.jpg

#define M1     26
#define INTR   28
#define MREQ   18  // Needs interrupt-enabled pin
#define IORQ   20  // Needs interrupt-enabled pin
#define WRITE 48
#define READ  49

#define BUSACK 14
#define HALT   15
#define BUSRQ  16
#define WAIT   17
#define NMI    10

#define CLK    19

//  13 = Blue, 12 = Green, 11 = Red (on 2560 XPro R3)
#define REDLED 11
#define GREENLED 12
#define BLUELED 13

#define SCREEN_PAGE_BUTTON 40
#define PAGE_CONTEXT_BUTTON 43
#define RESET_BUTTON 41


#define RESET    22
#define REFRESH  30

#define SDCARD_MISO   50
#define SDCARD_MOSI   51
#define SDCARD_CS     53
#define SDCARD_CLK    52

#endif
