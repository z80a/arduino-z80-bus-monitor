static SerialBridgeStatusPort Z80Memory::serialBridgeStatusPort = SerialBridgeStatusPort(0x03);

#include <SPI.h>
#include <SD.h>

static int Z80Memory::numberOfBlocks = 0;
static MemoryBlock Z80Memory::blocks[16]; // max 16 for now

Z80Memory::Z80Memory() {
  setupInterrupts();
}

void Z80Memory::begin() {
  setupMemory();
}


void Z80Memory::setupMemory() {
  parseConfig(F("MEMORY.CFG"));
}

void Z80Memory::parseConfig(String filename) {
  if (!SD.begin(SDCARD_CS)) {
    Console::error(F("Could not initialize SD-card"));
    oled.displayFullscreen("ERROR: Could not initialize SD-card");
    while(true);
    return;
  }


  File configFile = SD.open(filename, FILE_READ);
  if (!configFile) {
    Console::error(F("Could not read file '"));
    Console::error(filename);
    Console::error(F("'"));

    oled.displayFullscreen("ERROR: Could not read file");
    while(true);
    return;
  }

  oled.displayFullscreen(F("Reading MEMORY.CFG"));

  // Reading config file line by line
  String line;
  int lineNumber = 0;
  while (configFile.available()) {
    line = configFile.readStringUntil('\n');
    lineNumber++;
    if ((line.length() > 0) && (line[0] != '#')) {
      uint8_t atSignOffset = line.indexOf('@');
      String startAddress = line.substring(atSignOffset + 1, atSignOffset + 5);
      String filename = line.substring(0, atSignOffset);

      if (startAddress.length() != 4) {
        Console::error(F("Incorrect start address at line: "));
        Console::error(String(lineNumber));
        Console::error(F(" (I got: '"));
        Console::error(startAddress);
        Console::error(F("', line reads as: '"));
        Console::error(line);
        Console::error(F("')\n"));

        oled.displayFullscreen("ERROR: Incorrect start address");
        while(true);
        return;
      }

      if (filename.length() < 5) {
        Console::error(F("Incorrect filename at line: "));
        Console::error(String(lineNumber));
        Console::error(F(" (I got: '"));
        Console::error(filename);
        Console::error(F("', line reads as: '"));
        Console::error(line);
        Console::error(F("')\n"));
        oled.displayFullscreen("ERROR: Incorrect filename");
        while(true);
        return;
      }

      char address[4];
      startAddress.toCharArray(address, 4);
      uint16_t blockAddress;
      blockAddress = strtol(address, NULL, 16);

      blocks[numberOfBlocks] = MemoryBlock(filename, blockAddress);
      numberOfBlocks++;
    }
  }
  configFile.close();


  oled.displayFullscreen("Blocks found: " + String(numberOfBlocks));

  // Now read the binary files
  for (int i=0; i<numberOfBlocks; i++) {
    MemoryBlock block = Z80Memory::blocks[i];
    copyBinaryToMemory(block.filename, block.startAddress);
  }

}


void Z80Memory::readBinary(MemoryBlock& memoryBlock) {
  oled.displayFullscreen("Loading " + memoryBlock.filename);

  String filename = memoryBlock.filename;
  uint16_t startAddress = memoryBlock.startAddress;

  // Reading binary
    File file = SD.open(filename, FILE_READ);
    if (!file) {
      Console::error(F("Could not read file '"));
      Console::error(filename);
      Console::error(F("'\n"));
      return;
    }

    uint16_t fileSize = file.size();
    uint8_t* contents = (uint8_t*)malloc(fileSize);
    // Byte by byte b/c I had lockups with larger block sizes
    for (int i=0; i++; i<fileSize) {
      file.seek(i);
      file.read(&contents + i, 1);
    }

    memoryBlock.contents = contents;
    memoryBlock.size = fileSize;
    //free(contents); //don't free, give responsibility to the MemoryBlock to do that when needed

    file.close();
}

void Z80Memory::copyBinaryToMemory(String filename, uint16_t startAddress) {
  oled.displayFullscreen("Loading " + filename + " into physical RAM");
  Console::log("Loading " + filename + " into physical RAM at startaddress " + String(startAddress) + "\n");

  File file = SD.open(filename, FILE_READ);
  if (!file) {
    Console::error(F("Could not read file '"));
    Console::error(filename);
    Console::error(F("'\n"));
    return;
  }

  uint16_t fileSize = file.size();

  // TODO: Add boundary check

  // Byte by byte b/c I had lockups with larger block sizes
  uint8_t c = 0xff;
  for (int i=0; i++; i<fileSize) {
    file.seek(i);
    file.read(&c, 1);

    if (busMonitor.addressBus.claim()) {
      busMonitor.addressBus.write(startAddress + i);
      busMonitor.dataBus.write(c);
      busMonitor.addressBus.unclaim();
    } else {
      Console::error(F("Could not claim bus for writing to physical ram!"));
      return;
    }
  }

  file.close();
}

static bool Z80Memory::blockFromAddress(uint16_t address, MemoryBlock** blockFound) {
  for (int i=0; i<numberOfBlocks; i++) {
    MemoryBlock block = Z80Memory::blocks[i];

    if (block.addressIsInBlock(address)) {
      *blockFound = &block;
      return true;
    }
  }

  return false;
}

void Z80Memory::setupInterrupts() {
  attachInterrupt(digitalPinToInterrupt(MREQ), Z80Memory::memoryRequestInterrupt, FALLING);
  attachInterrupt(digitalPinToInterrupt(IORQ), Z80Memory::ioRequestInterrupt, FALLING);
}

// Don't use Serial or delay in an interrupt function
static void Z80Memory::memoryRequestInterrupt() {
  bool read = !digitalRead(READ);
  bool write = !digitalRead(WRITE);
  bool refresh = !digitalRead(REFRESH);

  // Skip memory refreshes
  if (refresh) {
    return;
  }

//  // 1. Begin WAIT
//  pinMode (WAIT, OUTPUT);
//  digitalWrite(WAIT, LOW);

  // 2. Do stuff

  if (write) {
    memoryWriteData.addressBus = busMonitor.addressBus.read();

//    // Assume that everything in upper half of memory is handled by physical RAM
//    if (memoryWriteData.addressBus >= 0x8000) {
//      return;
//    } else {
//      bool found = blockFromAddress(memoryWriteData.addressBus, *block);
  //    }

    memoryWriteData.dataBus = busMonitor.dataBus.read();
    lastBusData = memoryWriteData;
//    memory.ram[memoryWriteData.addressBus] = memoryWriteData.dataBus;

    // TODO:
    // block.write(memoryWriteData.addressBus, memoryWriteData.dataBus)
    displayNeedsUpdate = true;
  }

  if (read) {
    memoryReadData.addressBus = busMonitor.addressBus.read();

    MemoryBlock* block = NULL;
    bool found = blockFromAddress(memoryReadData.addressBus, &block);
    if (found) {
      (void)block->read(memoryReadData.addressBus, &memoryReadData.dataBus);
    } else {
      memoryReadData.dataBus = 0xFF; // RST38
    }

    lastBusData = memoryReadData;
    displayNeedsUpdate = true;

//    if (memoryReadData.addressBus < Z80Memory::RamSize) {
//      memoryReadData.dataBus = memory.ram[memoryReadData.addressBus];
//    // TODO:
//    // memoryReadData.dataBus = block.read(memoryReadData.addressBus)
//      lastBusData = memoryReadData;
//      displayNeedsUpdate = true;
//    } else {
//      memoryReadData.dataBus = 0x0; // nop
//      lastBusData = memoryReadData;
//      displayNeedsUpdate = true;
//    }

// Don't set the data on the bus if we have physical RAM installed
//    busMonitor.dataBus.write(memoryReadData.dataBus);

  }

  // 3. End WAIT
//  digitalWrite(WAIT, HIGH);
//  pinMode (WAIT, INPUT);
}


// Don't use Serial or delay in an interrupt function
static void Z80Memory::ioRequestInterrupt() {
  bool read = !digitalRead(READ);
  bool write = !digitalRead(WRITE);

  if (read) {
    ioReadData.addressBus = busMonitor.addressBus.read();

    uint8_t portNumber = ioReadData.addressBus && 0x00FF;
    ioReadData.dataBus = readInput(portNumber);
    lastBusData = ioReadData;
    displayNeedsUpdate = true;
    busMonitor.dataBus.write(ioReadData.dataBus);
  }

  if (write) {
    ioWriteData.addressBus = busMonitor.addressBus.read();
    ioWriteData.dataBus = busMonitor.dataBus.read();
    lastBusData = ioWriteData;
    // doing nothing for
    displayNeedsUpdate = true;
  }
}

void Z80Memory::reset() {
  memoryWriteData.addressBus = 0;
  memoryWriteData.dataBus = 0;
  memoryReadData.addressBus = 0;
  memoryReadData.dataBus = 0;
  ioWriteData.addressBus = 0;
  ioWriteData.dataBus = 0;
  ioReadData.addressBus = 0;
  ioReadData.dataBus = 0;
}

static uint8_t Z80Memory::readInput(uint8_t rawPortNumber) {
  IoPortNumber portNumber = IoPortNumber(rawPortNumber);

  switch (portNumber) {
    case IoPortNumber::SerialBridgeStatus:
      return serialBridgeStatusPort.read();
      break;
    case IoPortNumber::SerialBridgeData:
      // TODO: Connect
      break;
  }

  return 0x0;
}

static void Z80Memory::writeOutput(uint8_t rawPortNumber, uint8_t data) {
  IoPortNumber portNumber = IoPortNumber(rawPortNumber);

  switch (portNumber) {
    case IoPortNumber::SerialBridgeStatus:
      serialBridgeStatusPort.write(data);
      break;
    case IoPortNumber::SerialBridgeData:
      // TODO: Connect
      break;
  }
}
