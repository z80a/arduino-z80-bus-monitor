#include "Button.h"
#include "Console.h"

Button::Button(uint8_t pin) : _pin(pin) {}

void Button::begin() {
    pinMode(_pin, INPUT_PULLUP);
    _state = digitalRead(_pin);
}

ButtonState Button::buttonState() {
    bool state = digitalRead(_pin);

    if (state != _state) {
      lastDebounceTime = millis();
    }

    if ((millis() - lastDebounceTime) < debounceDelay) {
      return;
    }

    // Cancel button when long press timeout reached
    if ((buttonPressTime > 0) && !state && ((millis() - buttonPressTime) > longPressTime)) {
      Console::log(F("Long press"));
      buttonPressTime = 0;
      return LongPress;
    }

    if (state != _state) {
        _state = state;

        // Pressed
        if (!_state) {          
          buttonPressTime = millis();
          
        // Released
        } else {

          // Ignore long press (is handled above)
          if ((millis() - buttonPressTime) > longPressTime) {
            buttonPressTime = 0;
            return NotPressed;

          } else {
            Console::log(F("Short press"));
            buttonPressTime = 0;
            return ShortPress;
          }
        }
    }
    return NotPressed;
}
