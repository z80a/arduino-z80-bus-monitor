#ifndef MonitorDisplay_h
#define MonitorDisplay_h

#include "StatusViewModel.h"
#include "Screen.h"

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET -1 // Reset pin # (or -1 if sharing Arduino reset pin)

class MonitorDisplay {
  
  typedef enum ScreenConfigState: int {
    DontShow = 0,
    Show = 1,
    Highlight = 2,
  } ScreenConfigState_t;
  
  typedef struct ScreenConfiguration {
    ScreenConfigState m1 = DontShow;
    ScreenConfigState tState = DontShow;
    ScreenConfigState intr = DontShow;
    ScreenConfigState nmi = DontShow;
    ScreenConfigState reset = DontShow;
    ScreenConfigState halt = DontShow;
    ScreenConfigState rd = DontShow;
    ScreenConfigState wr = DontShow;
    ScreenConfigState refresh = DontShow;
    ScreenConfigState clk = DontShow;
    ScreenConfigState mreq = DontShow;
    ScreenConfigState iorq = DontShow;
    ScreenConfigState memoryRead = DontShow;
    ScreenConfigState memoryWrite = DontShow;
    ScreenConfigState ioRead = DontShow;
    ScreenConfigState ioWrite = DontShow;
    ScreenConfigState lastBusData = DontShow;
    ScreenConfigState wait = DontShow;
    ScreenConfigState busrq = DontShow;
    ScreenConfigState busack = DontShow;
    ScreenConfigState intrAck = DontShow;
  } ScreenConfiguration_t;

  public:
    MonitorDisplay();
    void setup();
    
    void displayFullscreen(String message);
//    void displayProgress(String title, float progress, String subtitle, float subprogress);
    void displayStatusPage(StatusViewModel viewModel);
    void nextScreen();
    
  private:
    void displaySignal(int x, int y, String name, bool signal, ScreenConfigState configuration);
    void displaySignalsScreen(String title, StatusViewModel viewModel, ScreenConfiguration configuration);
    void lightLed(StatusViewModel viewModel, Screen screen);
    void displaySignals(StatusViewModel viewModel);
    void displayMemoryRead(StatusViewModel viewModel);
    void displayMemoryWrite(StatusViewModel viewModel);
    void displayIoRead(StatusViewModel viewModel);
    void displayIoWrite(StatusViewModel viewModel);
    Screen currentPage = ScreenMin;
    void displayHex8(uint8_t *data, uint8_t length);
    void displayHex16(uint16_t *data, uint8_t length);
};
#endif
