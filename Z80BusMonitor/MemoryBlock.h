#ifndef MemoryBlock_h
#define MemoryBlock_h
/*
 * A memory block is an abstraction to save memory space on the Arduino.
 * 
 * There is not enough memory to simulate 64K (or even only 16K) for this Z80 project, but I still want to be able to
 * provide my Z80 with some routines. A memory block contains one or more routines and its starting address. All memory
 * blocks together define the Z80 memory, but the gaps in between don't take up any space on the Arduino. We'll just return
 * 0x00 (or 0xFF or some other value) when memory outside these block is accessed
 */

class MemoryBlock {
  public:
    uint16_t startAddress = 0;
    String filename;
    uint8_t* contents;
    uint16_t size = 0;

    MemoryBlock();
    MemoryBlock(String filename, uint16_t startingAddress);
    bool addressIsInBlock(uint16_t address);
    bool read(uint16_t address, uint8_t* data);
};

#endif
