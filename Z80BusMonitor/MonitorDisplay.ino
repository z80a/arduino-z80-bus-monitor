#include "MonitorDisplay.h"
#include <Fonts/FreeSans12pt7b.h>

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

MonitorDisplay::MonitorDisplay() {
}

void MonitorDisplay::setup() {
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    fatalError(F("SSD1306 allocation failed"));
  }

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  
//  display.setFont(&FreeSans12pt7b);
//  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  
  display.setCursor(0,0);
  display.print(F("Z80 Bus Monitor")); 
  display.display();
  
//  display.setFont();
}

void MonitorDisplay::nextScreen() {
  currentPage = currentPage +1;
  if (currentPage > ScreenMax) {
    currentPage = ScreenMin;
  }
}

void MonitorDisplay::displayFullscreen(String message) {
  display.setCursor(0,0);
  display.clearDisplay();
  display.println(message); 
  display.display();
}

void MonitorDisplay::displayHex8(uint8_t *data, uint8_t length) { // prints 8-bit data in hex with leading zeroes 
       for (int i=0; i<length; i++) {
         if (data[i]<0x10) {display.print("0");} 
         display.print(data[i],HEX); 
         display.print(" "); 
       }
}

void MonitorDisplay::displayHex16(uint16_t *data, uint8_t length) { // prints 16-bit data in hex with leading zeroes
       for (int i=0; i<length; i++)
       { 
         uint8_t MSB=byte(data[i]>>8);
         uint8_t LSB=byte(data[i]);
         
         if (MSB<0x10) {display.print("0");} display.print(MSB,HEX); display.print(" "); 
         if (LSB<0x10) {display.print("0");} display.print(LSB,HEX); display.print(" "); 
       }
}

void MonitorDisplay::displaySignal(int column, int row, String name, bool signal, ScreenConfigState configuration) {
  if (configuration == DontShow) {
    return;
  }
  
  int x = column * 6;
  int y = row * 8;
  
  display.setCursor(x, y);

  if (signal) {

    if (configuration == Highlight) {
      display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
    }

    if (configuration == Show) {
      display.setTextColor(SSD1306_WHITE, SSD1306_BLACK);
    } 

    display.print(name);

  }
}

void MonitorDisplay::lightLed(StatusViewModel viewModel, Screen screen) {
  if (!viewModel.ledsEnabled) {
    return;
  }
  
  switch (screen) {
    case ScreenCycle:
      if (viewModel.m1) {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, LOW);
      } else {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);        
      }
      break;
    
    case ScreenMemoryRead:
      if (viewModel.mreq && viewModel.rd) {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, LOW);
        digitalWrite(BLUELED, HIGH);
      } else {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);        
      }
      break;

    case ScreenMemoryWrite:
      if (viewModel.mreq && viewModel.wr) {
        digitalWrite(REDLED, LOW);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);
      } else {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);        
      }
      break;
        
    case ScreenIoRead:
      if (viewModel.iorq && viewModel.rd) {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, LOW);
        digitalWrite(BLUELED, HIGH);
      } else {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);        
      }
      break;

    case ScreenIoWrite:
      if (viewModel.iorq && viewModel.wr) {
        digitalWrite(REDLED, LOW);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);
      } else {
        digitalWrite(REDLED, HIGH);
        digitalWrite(GREENLED, HIGH);
        digitalWrite(BLUELED, HIGH);        
      }
      break;
        
    default:
      digitalWrite(REDLED, HIGH);
      digitalWrite(GREENLED, HIGH);
      digitalWrite(BLUELED, HIGH);
      break;
    }
}

void MonitorDisplay::displaySignalsScreen(String title, StatusViewModel viewModel, ScreenConfiguration configuration) {
  display.clearDisplay();

  // Line 0
  display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);
  display.setCursor(0,0);
  display.print(F("                     "));
  display.setCursor(((21.0 - title.length()) / 2.0) * 6, 0);
  display.print(title);

  // Line 1
  display.setTextColor(SSD1306_WHITE, SSD1306_BLACK);   
  if (configuration.memoryRead == Show || configuration.memoryWrite == Show || configuration.ioRead == Show || configuration.ioWrite == Show || configuration.lastBusData == Show) {
    display.setCursor(0, 9);

    if (configuration.ioRead == Show || configuration.ioWrite == Show) {
      display.print(F("PORT: "));
    } else {
      display.print(F("ADDR: "));
    }

    if (configuration.memoryRead == Show) displayHex16(&memoryReadData.addressBus, 1);
    if (configuration.memoryWrite == Show) displayHex16(&memoryWriteData.addressBus, 1);
    if (configuration.ioRead == Show) displayHex16(&ioReadData.addressBus, 1);
    if (configuration.ioWrite == Show) displayHex16(&ioWriteData.addressBus, 1);
    if (configuration.lastBusData == Show) displayHex16(&lastBusData.addressBus, 1);

    display.setCursor(70,9);
    display.print(F("DATA: "));
    if (configuration.memoryRead == Show) displayHex8(&memoryReadData.dataBus, 1);
    if (configuration.memoryWrite == Show) displayHex8(&memoryWriteData.dataBus, 1);
    if (configuration.ioRead == Show) displayHex8(&ioReadData.dataBus, 1);
    if (configuration.ioWrite == Show) displayHex8(&ioWriteData.dataBus, 1);
    if (configuration.lastBusData == Show) displayHex8(&lastBusData.dataBus, 1);
  }

  // Line 2
  displaySignal(0, 2, F("CLK"), viewModel.clk, configuration.clk);
  
  displaySignal(5, 2, F("M1"), viewModel.m1, configuration.m1);

  char str[20];
  sprintf(str, "T%d", viewModel.tState);
  displaySignal(8, 2, String(str), true, Show);

  displaySignal(14, 2, F("REFRESH"), viewModel.refresh, configuration.refresh);

  // Line 3
  displaySignal(0, 3, F("MREQ"), viewModel.mreq, configuration.mreq);
  displaySignal(6, 3, F("IORQ"), viewModel.iorq, configuration.iorq);
  displaySignal(11, 3, F("RD"), viewModel.rd, configuration.rd);
  displaySignal(15, 3, F("WR"), viewModel.wr, configuration.wr);
  displaySignal(18, 3, F("INTACK"), viewModel.intrAck, configuration.intrAck);

   // Line 4
  displaySignal(0, 4, F("RESET"), viewModel.reset, configuration.reset);
  displaySignal(7, 4, F("HALT"), viewModel.halt, configuration.halt);
  displaySignal(12, 4, F("INT"), viewModel.intr, configuration.intr);
  displaySignal(16, 4, F("NMI"), viewModel.nmi, configuration.nmi);

  // Line 5
  displaySignal(0, 5, F("WAIT"), viewModel.wait, configuration.wait);
  displaySignal(6, 5, F("BUSRQ"), viewModel.busrq, configuration.busrq);
  displaySignal(12, 5, F("BUSACK"), viewModel.busack, configuration.busack);

  display.display();

  lightLed(viewModel, currentPage);
}

void MonitorDisplay::displayStatusPage(StatusViewModel viewModel) {
  display.setTextSize(1);
  display.clearDisplay();

//display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);   
//display.setCursor(0,9);
//  if (viewModel.clk) display.print("C L O C K");
//  display.display();
//  return;
  ScreenConfiguration configuration;

  configuration.m1 = Show;
  configuration.tState = Show;
  configuration.refresh = Show;
  configuration.clk = Show;
  configuration.rd = Show;
  configuration.wr = Show;
  configuration.iorq = Show;
  configuration.mreq = Show;
  configuration.reset = Show;
  configuration.intr = Show;
  configuration.intrAck = Show;
  configuration.nmi = Show;
  configuration.halt = Show;
  configuration.wait = Show;
  configuration.busrq = Show;
  configuration.busack = Show;

  switch (currentPage) {
    case ScreenCycle:
      configuration.m1 = Highlight;
      configuration.lastBusData = Show;
      displaySignalsScreen(F("M1 CYCLE"), viewModel, configuration);
      break;

    case ScreenMemoryRead:
      configuration.rd = Highlight;
      configuration.mreq = Highlight;
      configuration.memoryRead = Show;
      displaySignalsScreen(F("MEMORY READ"), viewModel, configuration);
      break;

    case ScreenMemoryWrite:
      configuration.wr = Highlight;
      configuration.mreq = Highlight;
      configuration.memoryWrite = Show;
      displaySignalsScreen(F("MEMORY WRITE"), viewModel, configuration);
      break;

    case ScreenIoRead:
      configuration.rd = Highlight;
      configuration.iorq = Highlight;
      configuration.ioRead = Show;
      displaySignalsScreen(F("IO READ"), viewModel, configuration);
      break;

    case ScreenIoWrite:
      configuration.wr = Highlight;
      configuration.iorq = Highlight;
      configuration.ioWrite = Show;
      displaySignalsScreen(F("IO WRITE"), viewModel, configuration);
      break;

    default:
      display.setTextColor(SSD1306_BLACK, SSD1306_WHITE);   
      display.print(F("UNKNOWN SCREEN"));
      display.display();
      break;
  }
}
