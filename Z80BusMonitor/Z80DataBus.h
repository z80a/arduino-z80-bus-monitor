#ifndef Z80DataBus_h
#define Z80DataBus_h

class Z80DataBus {
  public:
    uint8_t read();
    void write(uint8_t);
};


#endif
