#ifndef InstructionData_h
#define InstructionData_h

typedef struct InstructionData {
  uint16_t addressBus;
  uint8_t dataBus;
} InstructionData;

#endif
