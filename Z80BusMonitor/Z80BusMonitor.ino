/*
  Z80 Bus monitor, meant for understanding Z80 signals

  By Maurice de Bijl
  Copyright 2020 by The Convenience Factory

  Work in Progress

*/

#include "PinDefinitions.h"

#include "Z80BusMonitor.h"

#include "InstructionData.h"
#include "MonitorDisplay.h"
#include "CycleCounter.h"
#include "Button.h"
#include "Console.h"

Z80BusMonitor::Z80BusMonitor() {}

void Z80BusMonitor::initializePins() {
  pinMode (BLUELED, OUTPUT);
  pinMode (REDLED, OUTPUT);
  pinMode (GREENLED, OUTPUT);

  pinMode (ADDRESS0, INPUT);
  pinMode (ADDRESS1, INPUT);
  pinMode (ADDRESS2, INPUT);
  pinMode (ADDRESS3, INPUT);
  pinMode (ADDRESS4, INPUT);
  pinMode (ADDRESS5, INPUT);
  pinMode (ADDRESS6, INPUT);
  pinMode (ADDRESS7, INPUT);
  pinMode (ADDRESS8, INPUT);
  pinMode (ADDRESS9, INPUT);
  pinMode (ADDRESS10, INPUT);
  pinMode (ADDRESS11, INPUT);
  pinMode (ADDRESS12, INPUT);
  pinMode (ADDRESS13, INPUT);
  pinMode (ADDRESS14, INPUT);
  pinMode (ADDRESS15, INPUT);

  pinMode (DATA0, OUTPUT);
  pinMode (DATA1, OUTPUT);
  pinMode (DATA2, OUTPUT);
  pinMode (DATA3, OUTPUT);
  pinMode (DATA4, OUTPUT);
  pinMode (DATA5, OUTPUT);
  pinMode (DATA6, OUTPUT);
  pinMode (DATA7, OUTPUT);

  dataBus.write(0x0);

  pinMode (M1, INPUT);
  pinMode (WRITE, INPUT);
  pinMode (READ, INPUT);
  pinMode (IORQ, INPUT);
  pinMode (MREQ, INPUT);
  pinMode (CLK, INPUT);
  pinMode (RESET, INPUT);

  pinMode (INTR, OUTPUT);
  pinMode (NMI, OUTPUT);
  pinMode (BUSRQ, OUTPUT);
  pinMode (WAIT, OUTPUT);
  pinMode (HALT, OUTPUT);

  digitalWrite(INTR, HIGH);
  digitalWrite(NMI, HIGH);
  digitalWrite(BUSRQ, HIGH);
  digitalWrite(WAIT, HIGH);
  digitalWrite(HALT, HIGH);

  pinMode (INTR, INPUT);
  pinMode (NMI, INPUT);
  pinMode (BUSRQ, INPUT);
  pinMode (WAIT, INPUT);
  pinMode (HALT, INPUT);

  digitalWrite(REDLED, HIGH);
  digitalWrite(GREENLED, HIGH);
  digitalWrite(BLUELED, HIGH);
}

void Z80BusMonitor::begin() {
  initializePins();
  memory.begin();
}

void Z80BusMonitor::startReset() {
  if (resetTimer > 0) return;
  resetTimer = millis();

  pinMode (RESET, OUTPUT);
  digitalWrite(RESET, LOW);
}

void Z80BusMonitor::handleReset() {

  if (millis() - resetTimer >= resetDuration) {
    resetTimer = 0;

    // Reset, only a pulse as my Arduino slow clock takes over and holds reset long enough
    digitalWrite(RESET, HIGH);
    pinMode (RESET, INPUT);
  }
}

void Z80BusMonitor::startIntr() {
  if (intrTimer > 0) return;
  intrTimer = millis();

  pinMode (INTR, OUTPUT);
  digitalWrite(INTR, LOW);
}

void Z80BusMonitor::handleIntr() {

  if (millis() - intrTimer >= intrDuration) {
    intrTimer = 0;
    digitalWrite(INTR, HIGH);
    pinMode (INTR, INPUT);
  }
}

void Z80BusMonitor::startWait() {
  if (waitTimer > 0) return;
  waitTimer = millis();

  pinMode (WAIT, OUTPUT);
  digitalWrite(WAIT, LOW);
}

void Z80BusMonitor::handleWait() {

  if (millis() - waitTimer >= waitDuration) {
    waitTimer = 0;
    digitalWrite(WAIT, HIGH);
    pinMode (WAIT, INPUT);
  }
}

void Z80BusMonitor::resetMemory() {
  memory.reset();
}

String programVersion = "0.5";

Button screenChangeButton(SCREEN_PAGE_BUTTON);
Button pageContextButton(PAGE_CONTEXT_BUTTON);
Button resetButton(RESET_BUTTON);

bool displayNeedsUpdate = false;
bool fatalErrorOccurred = false;
String fatalErrorMessage = "";
String command = "";

Z80BusMonitor busMonitor = Z80BusMonitor();
CycleCounter cycleCounter = CycleCounter();
MonitorDisplay oled = MonitorDisplay();
Console console = Console(oled);

StatusViewModel statusViewModel = StatusViewModel();


void fatalError(String errorMessage) {
  Console::error(errorMessage);
  oled.displayFullscreen(errorMessage);
  for(;;) {
    bool reset = !digitalRead(RESET);
    if (reset) {
      fatalErrorOccurred = false;
      break;
    }
  }
}


void setup() {
  console.begin();
  oled.setup();
  // TODO:
  // - Reset
  // - Wait for reset
  // - Feed only NOPs
  // - Initialize memory
  // - Reset
  busMonitor.startReset();
  busMonitor.begin();

  console.prompt();

  screenChangeButton.begin();
  pageContextButton.begin();
  resetButton.begin();

  displayNeedsUpdate = true;
}

void updatePins() {
  bool lastM1 = statusViewModel.m1;
  statusViewModel.m1 = !digitalRead(M1);
  if (lastM1 != statusViewModel.m1) {
    displayNeedsUpdate = true;
  }

  bool lastIntr = statusViewModel.intr;
  statusViewModel.intr = !digitalRead(INTR);
  if (lastIntr != statusViewModel.intr) {
    displayNeedsUpdate = true;
  }

  bool lastRd = statusViewModel.rd;
  statusViewModel.rd = !digitalRead(READ);
  if (lastRd != statusViewModel.rd) {
    displayNeedsUpdate = true;
  }

  bool lastWr = statusViewModel.wr;
  statusViewModel.wr = !digitalRead(WRITE);
  if (lastWr != statusViewModel.wr) {
    displayNeedsUpdate = true;
  }

  bool lastNmi = statusViewModel.nmi;
  statusViewModel.nmi = !digitalRead(NMI);
  if (lastNmi != statusViewModel.nmi) {
    displayNeedsUpdate = true;
  }

  bool lastIrq = statusViewModel.iorq;
  statusViewModel.iorq = !digitalRead(IORQ);
  if (lastIrq != statusViewModel.iorq) {
    displayNeedsUpdate = true;
  }

  bool lastMreq = statusViewModel.mreq;
  statusViewModel.mreq = !digitalRead(MREQ);
  if (lastMreq != statusViewModel.mreq) {
    displayNeedsUpdate = true;
  }

  bool lastHalt = statusViewModel.halt;
  statusViewModel.halt = !digitalRead(HALT);
  if (lastHalt != statusViewModel.halt) {
    displayNeedsUpdate = true;
  }

  bool lastClk = statusViewModel.clk;
  statusViewModel.clk = !digitalRead(CLK);
  if (lastClk != statusViewModel.clk) {
    displayNeedsUpdate = true;
  }

  bool lastReset = statusViewModel.reset;
  statusViewModel.reset = !digitalRead(RESET);
  if (lastReset != statusViewModel.reset) {
    displayNeedsUpdate = true;
  }

  bool lastRefresh = statusViewModel.refresh;
  statusViewModel.refresh = !digitalRead(REFRESH);
  if (lastRefresh != statusViewModel.refresh) {
    displayNeedsUpdate = true;
  }

  int lastTState = statusViewModel.tState;
  statusViewModel.tState = cycleCounter.tState;
  if (lastTState != statusViewModel.tState) {
    displayNeedsUpdate = true;
  }

  bool lastWait = statusViewModel.wait;
  statusViewModel.wait = !digitalRead(WAIT);
  if (lastWait != statusViewModel.wait) {
    displayNeedsUpdate = true;
  }

  bool lastBusRq = statusViewModel.busrq;
  statusViewModel.busrq = !digitalRead(BUSRQ);
  if (lastBusRq != statusViewModel.busrq) {
    displayNeedsUpdate = true;
  }

  bool lastBusAck = statusViewModel.busack;
  statusViewModel.busack = !digitalRead(BUSACK);
  if (lastBusAck != statusViewModel.busack) {
    displayNeedsUpdate = true;
  }

  // Interrupt ackknowledge = M1 + IORQ (https://gizmofoundry.com/project-z80-z80-cpu-pinout/)
  bool lastIntrAck = statusViewModel.intrAck;
  statusViewModel.intrAck = statusViewModel.m1 & statusViewModel.iorq;
  if (lastIntrAck != statusViewModel.intrAck) {
    displayNeedsUpdate = true;
  }

}


// True if handled
void showMemoryBlockOverview() {
  Console::log(F("Memory blocks:\n\n"));
  Console::log(F("\tfile\t\tstart\tsize\n"));
  Console::log(F("\t----\t\t-----\t----\n"));

  for (int i=0; i<busMonitor.memory.numberOfBlocks; i++) {
     MemoryBlock block = busMonitor.memory.blocks[i];

    Console::log(F("\t"));
    Console::log(block.filename);
    Console::log(F("\t\t"));
    Console::printHex16(&block.startAddress, 1);
    Console::log(F("\t"));
    Console::log(String(block.size, DEC));
    Console::log(F("\t\n"));
   }
}

bool handleCommand(String command) {
  if (command == "b") {
    showMemoryBlockOverview();
    return true;
  }

  return false;
}

void handleButtons() {

  // Page
  ButtonState buttonState = screenChangeButton.buttonState();
  if (buttonState == ShortPress) {
    oled.nextScreen();
  } else if (buttonState == LongPress) {
    statusViewModel.ledsEnabled = !statusViewModel.ledsEnabled;
  }

  // Context
//  buttonState = pageContextButton.buttonState();
//  if (buttonState == ShortPress) {
//    oled.nextPage();
//  } else if (buttonState == LongPress) {
//    //
//  }

  // Reset
  buttonState = resetButton.buttonState();
  if (buttonState == ShortPress) {
    busMonitor.startWait();
  } else if (buttonState == LongPress) {
    busMonitor.startReset();
  }
}


// the loop function runs over and over again forever
void loop() {
  updatePins();

  busMonitor.handleReset();
  busMonitor.handleIntr();
  busMonitor.handleWait();
  handleButtons();

  if (statusViewModel.reset) {
    busMonitor.resetMemory();
  }

  if (displayNeedsUpdate) {
    oled.displayStatusPage(statusViewModel);
    displayNeedsUpdate = false;
  }

  if (fatalErrorOccurred) {
    fatalError(fatalErrorMessage);
  }


  // Read console
  if (console.read(command)) {
    console.log("\n\n");
    bool handled = handleCommand(command);
    console.handleCommand(command, handled);
  }

//  Serial.print(free_memory()); Serial.println(F(" bytes free"));
}
