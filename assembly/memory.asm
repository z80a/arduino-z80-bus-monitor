;;
;; Bunch of routines to experiment with
;;
;; Maurice de Bijl
;; Copyright 2020 by The Convenience Factory
;;

;; RST0 / !RESET / Cold boot
  .org 0000h
  .seek 0000h
  ; ld a, 'A'
  ; out ($03), a
  ; out ($03), a
  ; out ($03), a

;; RST08 / Warm boot boot
  .org 0008h
  .seek 0008h

;; RST10
  .org 0010h
  .seek 0010h
;; RST18
  .org 0018h
  .seek 0018h

;; RST20
  .org 0020h
  .seek 0020h

;; RST28
  .org 0028h
  .seek 0028h

;; RST30
  .org 0030h
  .seek 0030h

;; RST38 / Interrupt & Uninitialized RAM
;; If memory gaps are filled with ffh then it will jump to here
  .org 0038h
  .seek 0038h
  ret

;; Non-Maskable Interrupt (NMI)
  .org 0066h
  .seek 0066h
  ret

;; CHGET: get 1 character
;; Parameter: character in register a
;; eg:   ld a,'A'
  .org 009Fh
  .seek 009Fh
  out (03h), a
  ret

;; CHPUT: put 1 character
  .org 00A2h
  .seek 00A2h
  in a, (03h)
  ret
